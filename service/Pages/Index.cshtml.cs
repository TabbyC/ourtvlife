using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace OurTVLife.Pages
{
    public class IndexModel : PageModel
    {
        public string Username { get; set; }

        public async Task OnGetAsync()
        {
            var authResult = await HttpContext.AuthenticateAsync();
            if (authResult != null) {
                Username = authResult.Principal.Identity.Name;
            }
        }
    }
}
using Microsoft.Extensions.Options;
using TMDbLib.Client;

namespace OurTVLife.Services {
    public class TMDbService : ITMDbService {

        public TMDbService(IOptions<TMDbOptions> options) {
            Client = new TMDbClient(options.Value.ApiKey);
        }

        public TMDbClient Client { get; }
    }

    public class TMDbOptions {
        public string ApiKey { get; set; }
    }
}
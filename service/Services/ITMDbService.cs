using TMDbLib.Client;

namespace OurTVLife.Services {
    public interface ITMDbService {
        TMDbClient Client {get;}
    }
}
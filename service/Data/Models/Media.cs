namespace OurTVLife.Data.Models
{
    using Newtonsoft.Json;

    public class Media
    {
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "tvdb_id")]
        public string TVDbId { get; set; }

        [JsonProperty(PropertyName = "seen")]
        public bool Seen { get; set; }

        [JsonProperty(PropertyName = "wanttosee")]
        public int WantToSee { get; set; }

        [JsonProperty(PropertyName = "type")]
        public MediaType Type { get; set; }
    }

    public enum MediaType {
        None,
        TV,
        Movie,
    }
}
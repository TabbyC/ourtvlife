namespace OurTVLife.Data.Constants {
    public static class ErrorStrings {
        public const string USERNAME_TOO_LONG = "Username cannot be more than 200 characters";
    } 
}